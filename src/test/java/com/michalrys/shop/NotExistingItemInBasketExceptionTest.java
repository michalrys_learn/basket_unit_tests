package com.michalrys.shop;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class NotExistingItemInBasketExceptionTest {

    @Test
    public void shouldKeepInformationAboutProduct() {
        //given
        Item productA = new Product("Butter", 5.5);
        String expectedMessage = "There is no item like: " + productA + " in the basket.";
        //when
        NotExistingItemInBasketException error = new NotExistingItemInBasketException(productA);

        //then
        Assert.assertEquals(expectedMessage, error.getMessage());
    }
}