package com.michalrys.shop;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    @Test
    public void shouldBeEqualsTwoProductsWithTheSameNameAndPrice() {
        //given
        Item productA = new Product("Milk", 5.80);
        Item productB = new Product("Milk", 5.80);

        //when
        boolean areEquals = productA.equals(productB);

        //then
        Assert.assertEquals(true, areEquals);
    }

    @Test
    public void shouldBeNotEqualsTwoProductsWithDifferentNameButWithTheSamePrice() {
        //given
        Item productA = new Product("Milk", 5.80);
        Item productB = new Product("Milk2", 5.80);

        //when
        boolean areEquals = productA.equals(productB);

        //then
        Assert.assertEquals(false, areEquals);
    }

    @Test
    public void shouldBeNotEqualsTwoProductsWithTheSameNameButWithDifferentPrice() {
        //given
        Item productA = new Product("Milk", 5.80);
        Item productB = new Product("Milk", 5.79);

        //when
        boolean areEquals = productA.equals(productB);

        //then
        Assert.assertEquals(false, areEquals);
    }

    @Test
    public void shouldBeNotEqualDefinedProductToEmptyOne() {
        //given
        Item productA = new Product("Milk", 5.80);
        Item productB = new Product("", 0);

        //when
        boolean areEquals = productA.equals(productB);

        //then
        Assert.assertEquals(false, areEquals);
    }
}