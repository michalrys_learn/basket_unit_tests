package com.michalrys.shop;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class BasketTest {

    private static final String BASKET_IS_EMPTY = "Basket is empty.";
    private ItemContainer basket;
    private Item productA;
    private Item productB;
    private Item productC;
    private Item productD;
    private Item productE;

    @Before
    public void setUp() {
        basket = new Basket();
        productA = new Product("Milk", 2.00);
        productB = new Product("Tea", 1.00);
        productC = new Product("Bread", 3.00);
        productD = new Product("Butter", 2.50);
        productE = new Product("Bear", 5.50);
    }

    @Test
    public void shouldAddProduct() {
        // given
        basket.add(productA);
        String expectedResults = "Products:\n1) " + productA.getName() + " = 1x " + productA.getPrice() + " PLN\n";
        // when
        String listOfItems = basket.getListOfItems();
        // then
        Assert.assertEquals(expectedResults, listOfItems);
    }

    //TODO here is test like in web example
    @Test
    public  void shouldAddProduct2() {
        //given
        Map<Item, Integer> tempCollector = new LinkedHashMap<>();
        tempCollector.put(productA, 1);

        //when
        basket.add(productA);

        //then
        //TODO: this has no sense -> I have to compare my collecting cup for items from basket - I do not want to
        //TODO: make it open for access for everyone by getter !
    }

    @Test
    public void shouldReturnEmptyListOfProducts() {
        //given
        String expectedResults = BASKET_IS_EMPTY;
        //when
        String listOfItems = basket.getListOfItems();
        //then
        Assert.assertEquals(expectedResults, listOfItems);
    }

    @Test
    public void shouldAddTwoTheSameProductsOneByOne() {
        // given
        basket.add(productA);
        basket.add(productA);
        String expectedResults = "Products:\n" +
                "1) " + productA.getName() + " = 2x " + productA.getPrice() + " PLN\n";
        // when
        String listOfItems = basket.getListOfItems();
        // then
        Assert.assertEquals(expectedResults, listOfItems);
    }

    @Test
    public void shouldAddTwoDifferentKindOfProductsAndKeepAddingOrder() {
        // given
        basket.add(productA);
        basket.add(productB);
        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 1x " + productA.getPrice() + " PLN\n" +
                "2) " + productB.getName() + " = 1x " + productB.getPrice() + " PLN\n";
        // when
        String listOfItems = basket.getListOfItems();
        // then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldAddFiveDifferentProductsOneByOneAndKeepAddingOrder() {
        // given
        basket.add(productA);
        basket.add(productB);
        basket.add(productC);
        basket.add(productD);
        basket.add(productE);

        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 1x " + productA.getPrice() + " PLN\n" +
                "2) " + productB.getName() + " = 1x " + productB.getPrice() + " PLN\n" +
                "3) " + productC.getName() + " = 1x " + productC.getPrice() + " PLN\n" +
                "4) " + productD.getName() + " = 1x " + productD.getPrice() + " PLN\n" +
                "5) " + productE.getName() + " = 1x " + productE.getPrice() + " PLN\n";
        // when
        String listOfItems = basket.getListOfItems();

        // then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldAddThreeTheSameProductInSingleStep() {
        // given
        basket.addSeveral(3, productA);
        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 3x " + productA.getPrice() + " PLN\n";
        // when
        String listOfItems = basket.getListOfItems();
        // then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldAddTheSameProductSeveralPerOneStepAndSinglePerStep() {
        //given
        basket.addSeveral(2, productA);
        basket.add(productA);
        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 3x " + productA.getPrice() + " PLN\n";
        //when
        String listOfItems = basket.getListOfItems();
        //then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldRemoveOneProductFromSeveralOfIt() throws NotExistingItemInBasketException {
        //given
        basket.addSeveral(3, productA);
        basket.remove(productA);
        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 2x " + productA.getPrice() + " PLN\n";
        //when
        String listOfItems = basket.getListOfItems();
        //then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldRemoveProductAndReturnBasketEmpty() throws NotExistingItemInBasketException {
        //given
        basket.addSeveral(1, productA);
        basket.remove(productA);
        String expectedList = BASKET_IS_EMPTY;
        //when
        String listOfItems = basket.getListOfItems();
        //then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldAddProductAfterCleaningBasketToZero() throws NotExistingItemInBasketException {
        //given
        basket.add(productA);
        basket.remove(productA);
        basket.add(productA);
        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 1x " + productA.getPrice() + " PLN\n";
        //when
        String listOfItems = basket.getListOfItems();
        //then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test(expected = NotExistingItemInBasketException.class)
    public void shouldCannotRemoveProductWhichDoesNotExistInBasket() {
        //given
        basket.add(productA);
        //when
        basket.remove(productB);
        //then
    }

    @Test(expected = NotExistingItemInBasketException.class)
    public void shouldCannotRemoveSeveralProductsForProductWhichDoesNotExistInBasket() {
        //given
        basket.addSeveral(3, productA);

        //when
        basket.removeSeveral(2, productB);

        //then
    }

    @Test
    public void shouldRemoveDefinedAmountOfProducts() {
        //given
        basket.addSeveral(5, productA);
        basket.removeSeveral(2, productA);
        String expectedList = "Products:\n" +
                "1) " + productA.getName() + " = 3x " + productA.getPrice() + " PLN\n";
        //when
        String listOfItems = basket.getListOfItems();

        //then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldClearBasketFromProductWhenAmountToRemoveIsGreaterThanAmountOfProductInBasket() {
        //given
        basket.addSeveral(5, productA);
        basket.add(productB);
        basket.removeSeveral(10, productA);
        String expectedList = "Products:\n" +
                "1) " + productB.getName() + " = 1x " + productB.getPrice() + " PLN\n";
        //when
        String listOfItems = basket.getListOfItems();
        //then
        Assert.assertEquals(expectedList, listOfItems);
    }

    @Test
    public void shouldCountBill() {
        //given
        basket.add(productA);
        basket.addSeveral(2, productB);
        double expectedBill = productA.getPrice() + productB.getPrice() * 2;
        //when
        double bill = basket.countBill();
        //then
        Assert.assertEquals(expectedBill, bill, 0.0);
    }

}