package com.michalrys.shop;

import java.util.LinkedHashMap;
import java.util.Map;

public class Basket implements ItemContainer {
    private static final String BASKET_IS_EMPTY = "Basket is empty.";
    private final Map<Item, Integer> items = new LinkedHashMap<>();

    @Override
    public void add(Item item) {
        Integer amount = isItemInBasket(item) ? 1 + items.get(item) : 1;
        items.put(item, amount);
    }

    @Override
    public void addSeveral(int amount, Item item) {
        Integer newAmount = isItemInBasket(item) ? amount + items.get(item) : amount;
        items.put(item, amount);
    }

    @Override
    public void remove(Item item) {
        if (isItemInBasket(item)) {
            Integer amount = items.get(item);
            items.put(item, --amount);
        } else {
            throw new NotExistingItemInBasketException(item);
        }
        removeItemWithAmountZeroOrLessThanZero(item);
    }

    @Override
    public void removeSeveral(int amount, Item item) {
        if (isItemInBasket(item)) {
            Integer currentAmount = items.get(item);
            int newAmount = currentAmount - amount <= 0 ? 0 : (currentAmount - amount);
            items.put(item, currentAmount - amount);
        } else {
            throw new NotExistingItemInBasketException(item);
        }
        removeItemWithAmountZeroOrLessThanZero(item);
    }

    @Override
    public String getListOfItems() {
        if (items.isEmpty()) {
            return BASKET_IS_EMPTY;
        }

        StringBuilder productList = new StringBuilder();
        productList.append("Products:\n");

        int i = 1;
        for (Item product : items.keySet()) {
            productList.append(i++ + ") " + product.getName() + " = " + items.get(product) + "x " + product.getPrice() + " PLN\n");
        }

        return productList.toString();
    }

    @Override
    public double countBill() {
        double sum = 0;

        for (Item product : items.keySet()) {
            sum += product.getPrice() * items.get(product);
        }

        return sum;
    }

    private boolean isItemInBasket(Item item) {
        return items.containsKey(item);
    }

    private boolean isAmountOfItemEqualsOrLessThanZero(Item item) {
        return items.get(item) <= 0;
    }

    private void removeItemWithAmountZeroOrLessThanZero(Item item) {
        if (isAmountOfItemEqualsOrLessThanZero(item)) {
            items.remove(item);
        }
    }
}
