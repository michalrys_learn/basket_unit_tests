package com.michalrys.shop;

public class NotExistingItemInBasketException extends RuntimeException {
    public NotExistingItemInBasketException(Item item) {
        super("There is no item like: " + item + " in the basket.");
    }
}
