package com.michalrys.shop;

public interface ItemContainer {
    void add(Item item);

    void addSeveral(int amount, Item item);

    void remove(Item item) throws NotExistingItemInBasketException;

    void removeSeveral(int amount, Item item) throws NotExistingItemInBasketException;

    String getListOfItems();

    double countBill();
}
