package com.michalrys.shop;

public interface Item {
    String getName();

    double getPrice();
}
