package com.michalrys.shop;

public class MainApp {
    public static void main(String[] args) {

        ItemContainer basket = new Basket();

        Item product1 = new Product("Milk", 5.00);
        Item product2 = new Product("Bread", 3.50);
        Item product3 = new Product("Egg", 2.00);
        Item product4 = new Product("Beer", 4.00);

        basket.add(product1);
        basket.add(product2);
        basket.add(product3);
        basket.addSeveral(4, product1);
        basket.add(product4);

        try {
            basket.remove(product4);
        } catch (NotExistingItemInBasketException notExistingItemInBasketException) {
            notExistingItemInBasketException.printStackTrace();
            System.out.println(notExistingItemInBasketException.getMessage());
        }

        try {
            basket.remove(new Product("Unkown" ,10.5));
        } catch (NotExistingItemInBasketException notExistingItemInBasketException) {
            //notExistingItemInBasketException.printStackTrace();
            System.out.println(notExistingItemInBasketException.getMessage());
        }

        try {
            basket.removeSeveral(2, product1);
        } catch (NotExistingItemInBasketException notExistingItemInBasketException) {
            //notExistingItemInBasketException.printStackTrace();
            System.out.println(notExistingItemInBasketException.getMessage());
        }

        String listOfItems = basket.getListOfItems();
        System.out.println(listOfItems);
        double totalPrice = basket.countBill();

        System.out.println(String.format("You have to pay: %.2f PLN", totalPrice));
    }
}
